# Teste para Java + JSF

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)
### Requisitos

- Google books (https://developers.google.com/books/) não utilizar lib
- JSF

### Diferencial

- Testes unitários
- Arquitetura do projeto

## Como participar?

1. Faça um fork deste repositório.
2. Desenvolver o teste.
3. Abra um Merge Request contra esse repositório e nos envie um e-mail com o link do MR.
4. Faremos nossa análise e te daremos um retorno.

## Detalhes da prova

### Critérios analisados

- Arquitetura do projeto (camadas)
- Aplicação de orientação a objeto
- Funcionalidades e funcionamento

### O que você deve desenvolver

- A prova consiste em criar uma interface em JSF para interação com o Google books.
- O objetivo é ter 2 telas, onde a primeira contém uma relação/lista dos livros escolhidos como favoritos, e a segunda que será acessada por um menu, terá um campo de pesquisa de livros. Essa pesquisa por sua vez irá diretamente filtrar a pesquisa na API do Google books, retornando uma lista de livros com a descrição, título e foto. Ao lado de cada livro, uma opção para salvá-lo como favorito. Somente os favoritos deverão ser salvos na base local para exibição na tela 1, lista de favoritos.
- Seu projeto deve também conter um arquivo README com a explicação das tecnologias utilizadas e as instruções para rodar.
- Descrever suas dificuldades e facilidades, bem como o número de horas de desenvolvimento.

### Funcionalidades

A App deve conter as seguintes funcionalidades:

1. Tela 1 - principal: Listagem de meus livros favoritos
2. Menu - Pesquisar: Pesquisar livros no Google books e exibir em tela (com a foto)
3. Salvar livros na lista de "meus favoritos" (localmente na base escolhida)
4. Editar e excluir livros favoritos
5. Exibir alerta de confirmaçao de exclusão de favorito

### Especificações técnicas

* O App deve se comunicar com o Google Books API

## Dúvidas? Envio da prova?
`testes@lyncas.net`

### Desde já obrigado pelo seu interesse e boa prova!